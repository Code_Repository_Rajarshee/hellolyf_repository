/**
 * 
 */
package com.hellolyf.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import com.hellolyf.utility.WaitElement;

/**
 * @author Rajarshee
 *
 */
public class Signup {

	WebDriver driver;
	WaitElement waitElement;
	
	@FindBy(xpath=("//input[@id='mailid']"))
	WebElement enteremail;
	@FindBy(xpath=("//button[@id='nxt_sign']"))
	WebElement next_stp_signup;
	@FindBy(xpath=("//button[@id='refferal']"))
	WebElement hear_abt_us;
	@FindBy(xpath=("//button[@id='firstName']"))
	WebElement firstName;
	@FindBy(xpath=("//button[@id='lastName']"))
	WebElement lastName;
	@FindBy(xpath=("//button[@id='dob']"))
	WebElement dob;
	@FindBy(xpath=("//input[@id='age']"))
	WebElement age;
	@FindBy(xpath=("//input[@id='mobileNo']"))
	WebElement mobileNo;
	@FindBy(xpath=("//span[contains(text(),'Send')]"))
	WebElement Send;
	@FindBy(xpath=("//input[@id='otpCode']"))
	WebElement otpCode;
	@FindBy(xpath=("//input[@id='pass']"))
	WebElement pass;
	@FindBy(xpath=("//input[@id='confirmpass']"))
	WebElement confirmpass;
	@FindBy(xpath=("//button[@id='signUpBtn']"))
	WebElement signUpBtn;
	
	
	public Signup(WebDriver driver) {
		this.driver = driver;
	}
	
	
	public boolean verifysignuppagedone(String emailid, String fname,String lname,String dobirthdmy,int year,int month,int gender,String mobno,String otp,String passval,String confmpass) {
		WaitElement.waitforElement(enteremail, 30, driver);
		WaitElement.waitTill(3000);
		enteremail.click();
		enteremail.sendKeys(emailid);
		next_stp_signup.click();
		Select select = new Select(hear_abt_us);
		select.selectByVisibleText("Media");
		firstName.click();
		firstName.sendKeys(fname);
		lastName.click();
		lastName.sendKeys(lname);
		dob.click();
		dob.sendKeys(dobirthdmy);
		age.click();
		age.sendKeys("year");
		select.selectByIndex(month);
		select.selectByIndex(gender);
		mobileNo.click();
		mobileNo.sendKeys(mobno);
		Send.click();
		otpCode.sendKeys(otp);
		pass.click();
		pass.sendKeys(passval);
		confirmpass.click();
		confirmpass.sendKeys(confmpass);
		signUpBtn.click();
		//datepick.click();
		//datepickyear.click();
		//prevdatepickyearrange.click();
		String Expectedurl = "http://www.hellolyf.com/index.xhtm";
		String Actualurl = driver.getCurrentUrl();
		if(Expectedurl.equals(Actualurl)) {
			return true;
		}else {
			return false;
		}
	}
}
