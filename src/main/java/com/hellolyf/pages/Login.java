/**
 * 
 */
package com.hellolyf.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.hellolyf.utility.WaitElement;

/**
 * @author Rajarshee
 *
 */
public class Login {

	WebDriver driver;
	WaitElement waitElement;

	@FindBy(xpath=("//a[contains(text(),'Login')]"))
	WebElement loginbtn;
	@FindBy(xpath=("//input[@id='email']"))
	WebElement email;
	@FindBy(xpath=("//input[@id='password']"))
	WebElement password;
	@FindBy(xpath=("//button[@class='btn btn-login btn-primary']"))
	WebElement Loginend;
	@FindBy(xpath=("//a[contains(text(),'Psychology')]"))
	WebElement Psychology;
	@FindBy(xpath=("//a[@class='big-2']"))
	WebElement clicknowPhychology;
	@FindBy(xpath=("//a[contains(text(),'Login here')]"))
	WebElement Login_here;
	@FindBy(xpath=("//button[@class='btn btn-primary dropdown-toggle']"))
	WebElement clickloggedinbutton;
	@FindBy(xpath=("//a[contains(text(),'Sign Out')]"))
	WebElement Sign_Out;
	@FindBy(xpath=("//a[contains(text(),'Sign Up')]"))
	WebElement Sign_Up_btn;
	//@FindBy(xpath=("//a[contains(text(),'Login with Google')]"))
	//WebElement Login_with_Google;
	//@FindBy(xpath=("//div[@id='profileIdentifier']"))
	//WebElement profileIdentifier;



	public Login(WebDriver driver) {
		this.driver = driver;
	}

	public void clickLoginbtn() {
		WaitElement.waitforElement(loginbtn, 30, driver);
		WaitElement.waitTill(3000);
		loginbtn.click();
	}


	public void enterUserName(String uname) {
		WaitElement.waitforElement(email, 30, driver);
		WaitElement.waitTill(3000);
		email.click();
		email.sendKeys(uname);
	}

	public void enterPassName(String pass) {
		WaitElement.waitforElement(password, 30, driver);
		WaitElement.waitTill(3000);
		password.click();
		password.sendKeys(pass);
	}


	public void clickloginendbtn() {
		WaitElement.waitforElement(Loginend, 50, driver);
		WaitElement.waitTill(3000);
		Loginend.click();
	}

	public boolean verifyvalidLogin() {
		String Expurl = "https://hellolyf.com/login.do";
		String Actualurl = driver.getCurrentUrl();
		WaitElement.waitTill(3000);
		if(Expurl.equals(Actualurl)) {
			return true;
		}else {
			return false;
		}
	}


	public boolean verifInvalidLogin() {
		String ExpPopupText = "Either user name or password is wrong.";
		String ActPopupText = driver.switchTo().alert().getText();
		//System.out.println(ActPopupText);
		WaitElement.waitTill(3000);
		driver.switchTo().alert().accept();
		WaitElement.waitTill(3000);
		if(ExpPopupText.equals(ActPopupText)) {
			return true;
		}else {
			return false;
		}
	}

	public boolean verifyPsychologylink() {
		WaitElement.waitforElement(Psychology, 30, driver);
		WaitElement.waitTill(3000);
		Psychology.click();
		String ExpURL = "http://www.hellolyf.com/psychology";
		String ActURL = driver.getCurrentUrl();
		if(ExpURL.equals(ActURL)) {
			return false;
		}else {
			return true;
		}
	}


	public void verifyafterclickingPsychologylinklogindone() {
		WaitElement.waitforElement(Psychology, 30, driver);
		WaitElement.waitTill(3000);
		Psychology.click();
		WaitElement.waitTill(3000);
		clicknowPhychology.click();
		WaitElement.waitTill(3000);
		Login_here.click();
		//WaitElement.waitTill(3000);
		//clickloggedinbutton.click();
		//WaitElement.waitTill(3000);
		//Sign_Out.click();
	}
	
	public boolean verifyafterPsychologylinklogindone() {
		String Exptedurl = "http://www.hellolyf.com/login.do";
		String ActualURL = driver.getCurrentUrl();
		WaitElement.waitTill(3000);
		if(Exptedurl.equals(ActualURL)) {
			return false;
		}else {
			return true;
		}
	}
	
	
	public boolean verifySignuplink() {
		WaitElement.waitforElement(Sign_Up_btn, 30, driver);
		WaitElement.waitTill(3000);
		Sign_Up_btn.click();
		String ExpUrl = "http://www.hellolyf.com/signup.xhtm";
		String ActUrl = driver.getCurrentUrl();
		if(ExpUrl.contains(ActUrl)) {
			return false;
		}else {
			return true;
		}
	}
	
	/*public Signup verifysignupbtnclicked() {
		WaitElement.waitforElement(loginbtn, 30, driver);
		WaitElement.waitTill(3000);
		loginbtn.click();
		//Login_with_Google.click();
		return PageFactory.initElements(driver, Signup.class);
	}

	public boolean verifyvalidlogin() {
		String Expurl = "https://hellolyf.com/authenticategoogle.do?code=4/vwFEftg_vS2zA9ScBkSeGg_"
				+ "xXrkRcIPBILrO01IWvZU5u37kq2CaJ9U2Cs80v3Egg_hqXZDI4ewa8UDdOzWGiY0&scope=email+openid+https:"
				+ "//www.googleapis.com/auth/userinfo.email&authuser="
				+ "0&prompt=none&session_state=e07c55a559f33fcd6b75e1770d33b331602f1bcf..f7c9";
		String Actualurl = driver.getCurrentUrl();
		if(Expurl.equals(Actualurl)) {
			return true;
		}else {
			return false;
		}
	}*/
}
