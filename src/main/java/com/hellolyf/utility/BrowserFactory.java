/**
 * 
 */
package com.hellolyf.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
 * @author Rajarshee
 * In browserFactory we give option for user 
 * to launch same application in different browser
 *
 */
public class BrowserFactory {

	static WebDriver driver;

	public static WebDriver startBrowserApp(String browsername , String url ) {

		if(browsername.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", ".\\chromedriver\\chromedriver.exe");
			driver = new ChromeDriver();
		}else if(browsername.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.firefox.driver", ".\\firefoxdriver\\geckodriver.exe");
			driver = new FirefoxDriver();
		}else if(browsername.equalsIgnoreCase("internetexplorer")) {
			System.setProperty("webdriver.firefox.driver", ".\\iedriver\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
		}
		driver.manage().window().maximize();
		driver.get(url);
		return driver;
	}


}
