/**
 * 
 */
package com.hellolyf.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Rajarshee
 *
 */
public class WaitElement {

	public static void waitforElement(WebElement element, int time, WebDriver driver) {

		WebDriverWait wait = new WebDriverWait(driver,time);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public static void waitTill(int time) {
		try{
			Thread.sleep(time);
		}catch(Exception e) {
			System.out.println(e);
		}
	}
}

