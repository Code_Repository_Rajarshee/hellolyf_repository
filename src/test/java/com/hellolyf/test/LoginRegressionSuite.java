/**
 * 
 */
package com.hellolyf.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.hellolyf.pages.Login;
import com.hellolyf.utility.BrowserFactory;

/**
 * @author Rajarshee
 *
 */
public class LoginRegressionSuite {

	WebDriver driver;
	BrowserFactory browserfactory;
	Login login;

	@BeforeMethod
	public void browserAppLaunch() {
		driver = BrowserFactory.startBrowserApp("chrome", "https://hellolyf.com/");
	}
	
	@Test(priority = 0 , enabled = true)
	public void verifyvalidlogin() {
		try {
			login = PageFactory.initElements(driver, Login.class);
			login.clickLoginbtn();
			login.enterUserName("rajarshee.bhattacharya@gmail.com");
			login.enterPassName("123456");
			login.clickloginendbtn();
			Assert.assertEquals(true, login.verifyvalidLogin());
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	@Test(priority = 1 , enabled = true)
	public void verifyInvalidlogin() {
		try {
			login = PageFactory.initElements(driver, Login.class);
			login.clickLoginbtn();
			login.enterUserName("rajarshee.bhattacharya1@gmail.com");
			login.enterPassName("12345");
			login.clickloginendbtn();
			Assert.assertEquals(true, login.verifInvalidLogin());
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	@Test(priority = 2 , enabled = true)
	public void verifyPsychologylink() {
		try {
			login = PageFactory.initElements(driver, Login.class);
			Assert.assertEquals(true, login.verifyPsychologylink());
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	@Test(priority = 3 , enabled = true)
	public void verifyafterclickingPsychologylinklogindone() {
		try {
			login = PageFactory.initElements(driver, Login.class);
			login.verifyafterclickingPsychologylinklogindone();
			login.enterUserName("rajarshee.bhattacharya@gmail.com");
			login.enterPassName("123456");
			login.clickloginendbtn();
			Assert.assertEquals(true, login.verifyafterPsychologylinklogindone());
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	@Test(priority = 4 , enabled = true)
	public void verifsignuplink() {
		try {
			login = PageFactory.initElements(driver, Login.class);
			Assert.assertEquals(true, login.verifySignuplink());
		}
		catch(Exception e) {
			System.out.println(e);
		}
	}
	
	
	@AfterMethod
	public void close() {
		driver.close();
	}
}
